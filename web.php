<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ClientController@actionlist');
Route::get('/getnewsteam','ClientController@getteamnew');
Route::get('index',['middleware'=>'role', function(){
return view('admin.index');
}]);
Route::get('/test',function(){
return view('test');
});

Route::get('/contact','ContactController@getcontact');

Route::get('/logout','AdminController@Logout');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/category','CategoryController@actionlistCate');
Route::get('/editcate/{id}','CategoryController@geteditcategory');
Route::post('/editcate','CategoryController@posteditcategory');
Route::get('/deletecate/{id}','CategoryController@deletecate');
Route::get('/addcate','CategoryController@getaddcategory');
Route::post('/addcate','CategoryController@postaddcategory');

Route::get('/feedback','FeedbackController@listfb');
Route::get('/deletefb/{id}','FeedbackController@deletefb');
Route::post('/addfeedback','FeedbackController@addfeedback');

Route::get('/team', 'TeamController@listteam');
Route::get('/deleteteam/{id}','TeamController@deleteteam');
Route::get('/addteam','TeamController@getaddteam');
Route::post('/addteam','TeamController@postaddteam');
Route::get('/editteam/{id}','TeamController@geteditteam');
Route::post('/editteam','TeamController@posteditnews');

Route::get('/whychoose','WhychooseusController@actionlistwhychooseus');
Route::get('/editwhychoose/{id}','WhychooseusController@geteditWhychooseus');
Route::post('/editwhychoose','WhychooseusController@posteditWhychooseus');
Route::get('/deletewhychoose/{id}','WhychooseusController@deletewhychooseus');
Route::get('/addwhychoose','WhychooseusController@getaddWhychooseus');
Route::post('/addwhychoose','WhychooseusController@postaddWhychooseus');

Route::get('/type_role','RoleController@listteam');
Route::get('/deleterole/{id}','RoleController@deleterole');
Route::get('/addrole','RoleController@getaddrole');
Route::post('/addrole','RoleController@postaddrole');
Route::get('/editrole/{id}','RoleController@geteditrole');
Route::post('/editrole','RoleController@posteditrole');

Route::get('/user','UserController@listuser');

Route::group(['middleware'=>'adminLogin'],  function() {
  Route::get('/deleteuser/{id}','UserController@deleteuser');
Route::get('/adduser','UserController@getadduser');
Route::post('/adduser','UserController@postadduser');
Route::get('/edituser/{id}','UserController@getedituser');
Route::post('/edituser','UserController@postedituser');
});



Route::get('/product','ProductController@listproduct');
Route::get('/deletepro/{id}','ProductController@deletepro');
Route::get('/addproduct','ProductController@getaddproduct');
Route::post('/addproduct','ProductController@postaddproduct');
Route::get('/editproduct/{id}','ProductController@geteditproduct');
Route::post('/editproduct','ProductController@posteditproduct');


Route::get('/slide','SlideController@listslide');
Route::get('/deleteslide/{id}','SlideController@deleteslide');
Route::get('/addslide','SlideController@getaddslide');
Route::post('/addslide','SlideController@postaddslide');
Route::get('/editslide/{id}','SlideController@geteditslide');
Route::post('/editslide','SlideController@posteditslide');

Route::get('/whyimage','WhyImageController@listWhyImage');
Route::get('/deletewhyimage/{id}','WhyImageController@deleteWhyImage');
Route::get('/addwhyimage','WhyImageController@getaddWhyImage');
Route::post('/addwhyimage','WhyImageController@postaddWhyImage');
Route::get('/editwhyimage/{id}','WhyImageController@geteditWhyImage');
Route::post('/editwhyimage','WhyImageController@posteditWhyImage');

Route::get('/news','NewsController@listnews');
Route::get('/deletenews/{id}','NewsController@deletenews');
Route::get('/addnews','NewsController@getaddnews');
Route::post('/addnews','NewsController@postaddnews');
Route::get('/editnews/{id}','NewsController@geteditnews');
Route::post('/editnews','NewsController@posteditnews');



Route::get('/addproduct','ProductController@getaddproduct');
Route::post('/addproduct','ProductController@postaddproduct');
Route::get('/editproduct/{id}','ProductController@geteditproduct');
Route::post('/editproduct','ProductController@posteditproduct');

Auth::routes();
Route::get('/editproduct','ProductController@posteditproduct');

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();
Route::any('logout','Auth\LoginController@logout');

Route::get('/home', 'HomeController@index')->name('home');
